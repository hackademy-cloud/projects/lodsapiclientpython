import setuptools
import os


VERSION = "0.0.6"
TESTING_VERSION = os.environ.get("TESTING_VERSION")

if TESTING_VERSION is not None:
    VERSION += ".dev{}".format(TESTING_VERSION)

setuptools.setup(
    name="lods_api_client",
    version=VERSION,
    author="Marco Schlicht",
    author_email="m.schlicht@hackademy.cloud",
    description="Learn on Demand Systems API Client in Python",
    packages=setuptools.find_packages(),
    install_requires=[
        'requests',
        'dacite'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8.2',
)

