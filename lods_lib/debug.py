_DEBUG = False


def enable_debug_output():
    global _DEBUG
    _DEBUG = True


def debug_print(*args, **kwargs):
    if _DEBUG:
        print(*args, **kwargs)