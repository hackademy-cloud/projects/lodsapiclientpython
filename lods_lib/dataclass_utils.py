from dataclasses import dataclass


no_default = object()


def no_default_dataclass(cls):
    "https://stackoverflow.com/questions/51575931/class-inheritance-in-python-3-7-dataclasses"
    def __post_init__(self):
        for key in cls.__annotations__:
            value = self.__getattribute__(key)
            if value is no_default:
                raise TypeError("__init__ missing 1 required argument: '{}'".format(key))
    cls.__post_init__ = __post_init__
    return dataclass(cls)
