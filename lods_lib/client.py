from dataclasses import asdict
from dacite import from_dict
from typing import Optional, Dict, Any

import requests

from lods_lib.api_methods.cancel import CancelParameters, CancelResponse
from lods_lib.api_methods.catalog import CatalogResponse, CatalogParameters
from lods_lib.api_methods.class_ import ClassResponse, ClassParameters
from lods_lib.api_methods.close_user_account import CloseUserAccountResponse, CloseUserAccountParameters
from lods_lib.api_methods.delete_class import DeleteClassResponse, DeleteClassParameters
from lods_lib.api_methods.delivery_regions import DeliveryRegionsResponse
from lods_lib.api_methods.details import DetailsResponse, DetailsParameters
from lods_lib.api_methods.get_evaluation_responses import GetEvaluationResponsesResponse, \
    GetEvaluationResponsesParameters
from lods_lib.api_methods.get_evaluation_responses_for_event import GetEvaluationResponsesForEventResponse, \
    GetEvaluationResponsesForEventParameters
from lods_lib.api_methods.get_or_create_class import GetOrCreateClassResponse, GetOrCreateClassParameters
from lods_lib.api_methods.lab_profile import LabProfileResponse, LabProfileParameters
from lods_lib.api_methods.latest_results import LatestResultsResponse, LatestResultsParameters
from lods_lib.api_methods.launch import LaunchResponse, LaunchParameters
from lods_lib.api_methods.launch_for_event import LaunchForEventResponse, LaunchForEventParameters
from lods_lib.api_methods.launch_for_event_by_external_id import LaunchForEventByExternalIDResponse, \
    LaunchForEventByExternalIDParameters
from lods_lib.api_methods.replacement_tokens import ReplacementTokensParameters, ReplacementTokensResponse
from lods_lib.api_methods.result import ResultParameters, ResultResponse
from lods_lib.api_methods.results import ResultsResponse, ResultsParameters
from lods_lib.api_methods.resume import ResumeResponse, ResumeParameters
from lods_lib.api_methods.running_and_saved_labs import RunningAndSavedLabsResponse
from lods_lib.api_methods.save import SaveResponse, SaveParameters
from lods_lib.api_methods.score_activities import ScoreActivitiesParameters, ScoreActivitiesResponse
from lods_lib.api_methods.send_notification import SendNotificationResponse, SendNotificationParameters
from lods_lib.api_methods.stats import StatsResponse
from lods_lib.api_methods.update_class import UpdateClassResponse, UpdateClassParameters
from lods_lib.api_methods.update_lab_instructions import UpdateLabInstructionsResponse, UpdateLabInstructionsParameters
from lods_lib.api_methods.update_lab_instructions_package import UpdateLabInstructionsPackageParameters, \
    UpdateLabInstructionsPackageResponse
from lods_lib.api_methods.user_running_and_saved_labs import UserRunningAndSavedLabsResponse, \
    UserRunningAndSavedLabsParameters
from lods_lib.debug import debug_print
from lods_lib.errors.lods_errors import LodsResponseError, LodsResponseException


def _dict_fac(json_object):
    json_object_out = {}
    for key_old, value in json_object:
        if value is None:
            continue
        key = key_old
        if "_" in key:
            key = key.replace("_", "-")
        json_object_out[key] = value
    return json_object_out


def _make_response(response, response_cls):
    try:
        json_object = response.json()
        debug_print(json_object)
        response_obj = from_dict(data_class=response_cls, data=json_object)
        return response_obj
    except Exception as error1:
        # result is not the expected structure, maybe it's a lods error
        try:
            error = LodsResponseError(**json_object)
        except Exception as error2:
            # can't parse the result
            print(response)
            print(response.text)
            print(error1)
            print(error2)
            raise LodsResponseException(response, error1, error2)
        raise error


class ILodsApiClient:

    def api_call(self, method_name, params, response_cls):
        raise NotImplementedError()

    def cancel(self, params: CancelParameters) -> CancelResponse:
        """The Cancel command allows you to cancel a specified lab instance."""
        raise NotImplementedError()

    def catalog(self, params: Optional[CatalogParameters]=None) -> CatalogResponse:
        """The Catalog command will return all lab series, lab profiles, and delivery regions available to your
        organization. Lab profiles are generally grouped into series. Depending on your organization, you may have
        multiple physical delivery regions available to you."""
        raise NotImplementedError()

    def class_(self, params: ClassParameters) -> ClassResponse:
        """The Class command returns information about a class."""
        raise NotImplementedError()

    def close_user_account(self, params: CloseUserAccountParameters) -> CloseUserAccountResponse:
        """The CloseUserAccount command closes a user account and removes all user information using an ID. This is
        usually a unique identifier used by the calling system."""
        raise NotImplementedError()

    def delete_class(self, params: DeleteClassParameters) -> DeleteClassResponse:
        """The DeleteClass command deletes a specified class."""
        raise NotImplementedError()

    def delivery_regions(self) -> DeliveryRegionsResponse:
        """The DeliveryRegions command will return all delivery regions available to your organization."""
        raise NotImplementedError()

    def details(self, params: DetailsParameters) -> DetailsResponse:
        """The Details command retrieves detailed information about a specified lab instance."""
        raise NotImplementedError()

    def get_evaluation_responses_for_event(self, params: GetEvaluationResponsesForEventParameters) -> GetEvaluationResponsesForEventResponse:
        """The GetEvaluationResponsesForEvent command returns evaluation responses for a specific event."""
        raise NotImplementedError()

    def get_evaluation_responses(self, params: GetEvaluationResponsesParameters) -> GetEvaluationResponsesResponse:
        """The GetEvaluationResponses command returns evaluation responses."""
        raise NotImplementedError()

    def get_or_create_class(self, params: GetOrCreateClassParameters) -> GetOrCreateClassResponse:
        """The GetOrCreateClass command returns information about a class. If the class doesn’t exist, it is created."""
        raise NotImplementedError()

    def lab_profile(self, params: LabProfileParameters) -> LabProfileResponse:
        """The LabProfile command will return information about a specific lab profile."""
        raise NotImplementedError()

    def latest_results(self, params: LatestResultsParameters) -> LatestResultsResponse:
        """The LatestResults command returns information about all lab instance results that have recently changed
        state."""
        raise NotImplementedError()

    def launch_for_event_by_external_id(self, params: LaunchForEventByExternalIDParameters) -> LaunchForEventByExternalIDResponse:
        raise NotImplementedError()

    def launch_for_event(self, params: LaunchForEventParameters) -> LaunchForEventResponse:
        """The LaunchForEvent command launches a lab instance within an event."""
        raise NotImplementedError()

    def launch(self, params: LaunchParameters) -> LaunchResponse:
        """The Launch command will launch a specified lab for a specified user."""
        raise NotImplementedError()

    def replacement_tokens(self, params: ReplacementTokensParameters) -> ReplacementTokensResponse:
        """The ReplacementTokens command retrieves all @lab replacement tokens available for a particular lab
        instance."""
        raise NotImplementedError()

    def result(self, params: ResultParameters) -> ResultResponse:
        """The Result command returns information about a particular lab instance result."""
        raise NotImplementedError()

    def results(self, params: ResultsParameters) -> ResultsResponse:
        """The Results command returns information about all lab instance results that started or ended within a
        specified time range. The time range cannot exceed 7 days."""
        raise NotImplementedError()

    def resume(self, params: ResumeParameters) -> ResumeResponse:
        """The Resume command allows you to resume a specified lab instance."""
        raise NotImplementedError()

    def running_and_saved_labs(self) -> RunningAndSavedLabsResponse:
        """The RunningAndSavedLabs command retrieves all lab instances launched by this API Consumer that are currently
        running or saved."""
        raise NotImplementedError()

    def save(self, params: SaveParameters) -> SaveResponse:
        """The Save command allows you to save a specified lab instance."""
        raise NotImplementedError()

    def score_activities(self, params: ScoreActivitiesParameters) -> ScoreActivitiesResponse:
        """The ScoreActivities command causes all scored activities in a particular lab instance to undergo scoring.
        Please note that this is API command is only necessary in specialized situations. In most cases, scoring is
        triggered by the student in the lab client and this command is not needed. However, if your students do not
        use our lab client, ScoreActivities provides a mechanism to trigger scoring.

        This command does not return scoring results. To obtain scoring results,
        use the @Html.ActionLink("Details", "Details") command."""
        raise NotImplementedError()

    def send_notification(self, params: SendNotificationParameters) -> SendNotificationResponse:
        """The SendNotification command allows you to send a notification to a specified lab instance for the user
        to see."""
        raise NotImplementedError()

    def stats(self) -> StatsResponse:
        """The Stats command returns statistics about how many labs are currently active and saved."""
        raise NotImplementedError()

    def update_class(self, params: UpdateClassParameters) -> UpdateClassResponse:
        """The UpdateClass command updates a specified class."""
        raise NotImplementedError()

    def update_lab_instructions(self, params: Optional[UpdateLabInstructionsParameters]=None) -> UpdateLabInstructionsResponse:
        """The UpdateLabInstructions command allows you to update the IDL-MD instructions of a lab. In order to support
        longer instructions lengths, this method requires an HTTP POST. The Content-Type of your post should be set to
        application/x-www-form-urlencoded."""
        raise NotImplementedError()

    def update_lab_instructions_package(self, params: Optional[UpdateLabInstructionsPackageParameters]=None) -> UpdateLabInstructionsPackageResponse:
        """The UpdateLabInstructionsPackage command allows you to update the IDL-MD instructions of a lab by uploading
        a ZIP archive containing the instructions and any referenced files, such as images and videos. This method
        requires an HTTP POST. The Content-Type of your post should be set to multipart/form-data. This is the same
        behavior that a browser uses to upload a file to a server. In .NET environments, the System.Net.WebClient
        class provides an UploadFile method that can be used."""
        raise NotImplementedError()

    def user_running_and_saved_labs(self, params: UserRunningAndSavedLabsParameters) -> UserRunningAndSavedLabsResponse:
        """The UserRunningAndSavedLabs command retrieves all labs that are currently running or saved for a particular
        user account."""
        raise NotImplementedError()


class LodsApiClient(ILodsApiClient):

    def __init__(self, api_key: str, api_url: str="https://labondemand.com/api/v3/{method}"):
        self.api_key = api_key
        self.api_url = api_url

    def _get_method(self, method: str, params: Optional[Any]):
        if params is None:
            return self._get_method_unwrapped(method)
        json_object = asdict(params, dict_factory=_dict_fac)
        return self._get_method_unwrapped(method, json_object)

    def _get_method_unwrapped(self, method: str, params: Optional[Dict[str, str]] = None):
        url = self.api_url.format(method=method)
        headers = {"api_key": self.api_key}
        return requests.get(url, params=params, headers=headers)

    def _post_method_unwrapped(self, method: str, params: Optional[Dict[str, str]] = None, data: Optional[Dict[str, str]] = None):
        """Used to create or update resources.

        Untested!
        Docs: https://docs.learnondemandsystems.com/lod/how-to-use-api-consumer.md
        """
        raise NotImplementedError()
        url = self.api_url.format(method=method)
        headers = {"api_key": self.api_key}
        # data = urllib.urlencode(values, "utf-8")  # is this needed?
        return requests.post(url, params=params, data=data, headers=headers)

    def api_call(self, method_name, params, response_cls):
        resp = self._get_method(method_name, params)
        return _make_response(resp, response_cls)

    def cancel(self, params: CancelParameters) -> CancelResponse:
        return self.api_call("cancel", params, CancelResponse)

    def catalog(self, params: Optional[CatalogParameters]=None) -> CatalogResponse:
        return self.api_call("catalog", params, CatalogResponse)

    def class_(self, params: ClassParameters) -> ClassResponse:
        return self.api_call("class", params, ClassResponse)

    def close_user_account(self, params: CloseUserAccountParameters) -> CloseUserAccountResponse:
        return self.api_call("closeuseraccount", params, CloseUserAccountResponse)

    def delete_class(self, params: DeleteClassParameters) -> DeleteClassResponse:
        return self.api_call("deleteclass", params, DeleteClassResponse)

    def delivery_regions(self) -> DeliveryRegionsResponse:
        return self.api_call("deliveryregions", None, DeliveryRegionsResponse)

    def details(self, params: DetailsParameters) -> DetailsResponse:
        return self.api_call("details", params, DetailsResponse)

    def get_evaluation_responses_for_event(self, params: GetEvaluationResponsesForEventParameters) -> GetEvaluationResponsesForEventResponse:
        return self.api_call("getevaluationresponsesforevent", params, GetEvaluationResponsesForEventResponse)

    def get_evaluation_responses(self, params: GetEvaluationResponsesParameters) -> GetEvaluationResponsesResponse:
        return self.api_call("getevaluationresponses", params, GetEvaluationResponsesResponse)

    def get_or_create_class(self, params: GetOrCreateClassParameters) -> GetOrCreateClassResponse:
        return self.api_call("getorcreateclass", params, GetOrCreateClassResponse)

    def lab_profile(self, params: LabProfileParameters) -> LabProfileResponse:
        return self.api_call("labprofile", params, LabProfileResponse)

    def latest_results(self, params: LatestResultsParameters) -> LatestResultsResponse:
        return self.api_call("latestresults", params, LatestResultsResponse)

    def launch_for_event_by_external_id(self, params: LaunchForEventByExternalIDParameters) -> LaunchForEventByExternalIDResponse:
        return self.api_call("launchforeventbyexternalid", params, LaunchForEventByExternalIDResponse)

    def launch_for_event(self, params: LaunchForEventParameters) -> LaunchForEventResponse:
        return self.api_call("launchforevent", params, LaunchForEventResponse)

    def launch(self, params: LaunchParameters) -> LaunchResponse:
        return self.api_call("launch", params, LaunchResponse)

    def replacement_tokens(self, params: ReplacementTokensParameters) -> ReplacementTokensResponse:
        return self.api_call("replacementtokens", params, ReplacementTokensResponse)

    def result(self, params: ResultParameters) -> ResultResponse:
        return self.api_call("result", params, ResultResponse)

    def results(self, params: ResultsParameters) -> ResultsResponse:
        return self.api_call("results", params, ResultsResponse)

    def resume(self, params: ResumeParameters) -> ResumeResponse:
        return self.api_call("resume", params, ResumeResponse)

    def running_and_saved_labs(self) -> RunningAndSavedLabsResponse:
        return self.api_call("runningandsavedlabs", None, RunningAndSavedLabsResponse)

    def save(self, params: SaveParameters) -> SaveResponse:
        return self.api_call("save", params, SaveResponse)

    def score_activities(self, params: ScoreActivitiesParameters) -> ScoreActivitiesResponse:
        return self.api_call("scoreactivities", params, ScoreActivitiesResponse)

    def send_notification(self, params: SendNotificationParameters) -> SendNotificationResponse:
        return self.api_call("sendnotification", params, SendNotificationResponse)

    def stats(self) -> StatsResponse:
        return self.api_call("stats", None, StatsResponse)

    def update_class(self, params: UpdateClassParameters) -> UpdateClassResponse:
        return self.api_call("updateclass", params, UpdateClassResponse)

    def update_lab_instructions(self, params: Optional[UpdateLabInstructionsParameters]=None) -> UpdateLabInstructionsResponse:
        # TODO this needs POST and custom header
        raise NotImplementedError()
        return self.api_call("updatelabinstructions", params, UpdateLabInstructionsResponse)

    def update_lab_instructions_package(self, params: Optional[UpdateLabInstructionsPackageParameters]=None) -> UpdateLabInstructionsPackageResponse:
        # TODO this needs POST and custom header
        raise NotImplementedError()
        return self.api_call("updatelabinstructionspackage", params, UpdateLabInstructionsPackageResponse)

    def user_running_and_saved_labs(self, params: UserRunningAndSavedLabsParameters) -> UserRunningAndSavedLabsResponse:
        return self.api_call("userrunningandsavedlabs", params, UserRunningAndSavedLabsResponse)