from enum import Enum


class DetailsResponseLastSaveTriggerTypeEnum(Enum):
    UNKNOWN = "Unknown"
    BY_STUDENT = "By Student"
    BY_ADMINISTRATOR = "By Administrator"
    AUTOMATIC = "Automatic"
    FROM_API_CONSUMER = "From API Consumer"


