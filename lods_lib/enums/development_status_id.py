from enum import Enum


class DevelopmentStatusId(Enum):
    IN_DEVELOPMENT = (1, "In Development")
    AWAITING_VERIFICATION = (5, "AwaitingVerification")
    IN_VERIFICATION = (7, "InVerification")
    VERIFICATION_FAILED = (8, "VerificationFailed")
    COMPLETE = (10, "Complete")


