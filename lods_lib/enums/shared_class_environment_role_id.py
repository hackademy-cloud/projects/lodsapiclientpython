from enum import Enum


class SharedClassEnvironmentRoleId(Enum):
    NONE = (0, None)                                # This lab has no shared environment involvement at all. Most labs work this way.)
    SHARED_ENVIRONMENT = (10, "Shared Environment") # This lab provides the shared infrastructure that participant labs will connect into. Typically launched and maintained by an administrator or instructor.
    PARTICIPANT = (20, "Participant")               # This lab will connect into shared environments and act as a participant. Typically launched by students.


