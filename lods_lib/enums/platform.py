from enum import Enum


class Platform(Enum):
    HYPER_V = "Hyper-V"
    VIRTUAL_SERVER = "Virtual Server"
    VSPHERE = "vSphere"


