from enum import Enum


class PlatformId(Enum):
    NONE = (-1, None)
    HYPER_V = (2, "Hyper - V")
    VSPHERE = (3, "vSphere")
    AZURE = (10, "Azure")
    AWS = (11, "AWS")
    DOCKER = (20, "Docker")


