from enum import Enum


class CompletionStatus(Enum):
    CANCELLED = "Cancelled"
    NO_SHOW = "No Show"
    INCOMPLETE = "Incomplete"
    COMPLETE = "Complete"
    STORAGE_PROVISIONING_FAILED = "Storage Provisioning Failed"
    LAB_CREATION_FAILED = "Lab Creation Failed"
    RESUME_FAILED = "Resume Failed"
    SAVE_FAILED = "Save Failed"
    SUBMITTED_FOR_GRADING = "Submitted For Grading"
    GRADING_IN_PROGRESS = "Grading In Progress"