from enum import Enum


class State(Enum):
    OFF = "Off"
    PREPARING_CACHE = "Preparing Cache"
    BUILDING_VIRTUAL_ENVIRONMENT = "Building Virtual Environment"
    STARTING = "Starting"
    RUNNING = "Running"
    SAVING = "Saving"
    SAVED = "Saved"
    RESUMING = "Resuming"
    CREATING_SNAPSHOT = "Creating Snapshot"
    APPLYING_SNAPSHOT = "Applying Snapshot"
    SAVING_ENVIRONMENT = "Saving Environment"
    TEARING_DOWN = "Tearing Down"


