from enum import Enum


class ExamQuestionType(Enum):
    MULTIPLE_CHOICE_SINGLE_ANSWER = "MultipleChoiceSingleAnswer"
    MULTIPLE_CHOICE_MULTIPLE_ANSWER = "MultipleChoiceMultipleAnswer"
    TEXT_EXACT_WORD = "TextExactWord"
    TEXT_REGEX_MATCH = "TextRegexMatch"