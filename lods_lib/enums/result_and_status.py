from enum import Enum


class CloseUserAccountResponseStatus(Enum):
    ERROR = (0, "Error")
    SUCCESS = (1, "Success")


class CancelResponseResult(Enum):
    ERROR = (0, "Error")
    SUCCESS = (1, "Success")


class SendNotificationResponseResult(Enum):
    ERROR = (0, "Error")
    SUCCESS = (1, "Success")


class UpdateLabInstructionsResponseResult(Enum):
    ERROR = (0, "Error")
    SUCCESS = (1, "Success")


class UpdateLabInstructionsPackageResponseResult(Enum):
    ERROR = (0, "Error")
    SUCCESS = (1, "Success")


class SaveResponseResult(Enum):
    UNKNOWN_ERROR = (0, "Unknown Error")
    SUCCESS = (1, "Success")
    INVALID_STATE = (2, "Invalid State")


class LaunchForEventResponseResult(Enum):
    ERROR = (0, "Error")
    SUCCESS = (1, "Success")
    USER_HAS_TOO_MANY_ACTIVE_LABS = (2, "User has too many active labs")
    INSUFFICIENT_HOST_RESOURCES = (3, "Insufficient host resources")
    API_INTEGRATION_HAS_TOO_MANY_ACTIVE_LABS = (5, "API integration has too many active labs")
    USER_HAS_A_SAVED_INSTANCE_OF_THIS_LAB = (6, "User has a saved instance of this lab")
    API_INTEGRATION_DOESNT_HAVE_ENOUGH_AVAILABLE_RAM = (7, "API integration doesn't have enough available RAM")
    USER_DOESNT_HAVE_ENOUGH_AVAILABLE_RAM = (10, "User doesn't have enough available RAM")
    USERS_ORGANIZATION_HAS_TOO_MANY_ACTIVE_LABS = (20, "User's organization has too many active labs")
    USERS_ORGANIZATION_DOESNT_HAVE_ENOUGH_AVAILABLE_RAM = (30, "User's organization doesn't have enough available RAM")
    LAB_PROFILE_HAS_TOO_MANY_ACTIVE_INSTANCES = (40, "Lab profile has too many active instances")
    LAB_ORGANIZATION_DOESNT_HAVE_ENOUGH_AVAILABLE_RAM = (50, "Lab organization doesn't have enough available RAM")
    LAB_ORGANIZATION_HAS_TOO_MANY_ACTIVE_INSTANCES = (60, "Lab organization has too many active instances")
    LAB_SERIES_HAS_TOO_MANY_ACTIVE_INSTANCES = (70, "Lab series has too many active instances")
    LAB_SERIES_DOESNT_HAVE_ENOUGH_AVAILABLE_RAM = (80, "Lab series doesn't have enough available RAM")


LaunchForEventByExternalIDResponseResult = LaunchForEventResponseResult
ResumeResponseResult = LaunchForEventResponseResult


class LaunchResponseResult(Enum):
    """Is the same like LaunchForEventResponseResult, but with more keys."""
    ERROR = (0, "Error")
    SUCCESS = (1, "Success")
    USER_HAS_TOO_MANY_ACTIVE_LABS = (2, "User has too many active labs")
    INSUFFICIENT_HOST_RESOURCES = (3, "Insufficient host resources")
    API_INTEGRATION_HAS_TOO_MANY_ACTIVE_LABS = (5, "API integration has too many active labs")
    USER_HAS_A_SAVED_INSTANCE_OF_THIS_LAB = (6, "User has a saved instance of this lab")
    API_INTEGRATION_DOESNT_HAVE_ENOUGH_AVAILABLE_RAM = (7, "API integration doesn't have enough available RAM")
    USER_DOESNT_HAVE_ENOUGH_AVAILABLE_RAM = (10, "User doesn't have enough available RAM")
    USERS_ORGANIZATION_HAS_TOO_MANY_ACTIVE_LABS = (20, "User's organization has too many active labs")
    USERS_ORGANIZATION_DOESNT_HAVE_ENOUGH_AVAILABLE_RAM = (30, "User's organization doesn't have enough available RAM")
    LAB_PROFILE_HAS_TOO_MANY_ACTIVE_INSTANCES = (40, "Lab profile has too many active instances")
    LAB_ORGANIZATION_DOESNT_HAVE_ENOUGH_AVAILABLE_RAM = (50, "Lab organization doesn't have enough available RAM")
    LAB_ORGANIZATION_HAS_TOO_MANY_ACTIVE_INSTANCES = (60, "Lab organization has too many active instances")
    LAB_SERIES_HAS_TOO_MANY_ACTIVE_INSTANCES = (70, "Lab series has too many active instances")
    LAB_SERIES_DOESNT_HAVE_ENOUGH_AVAILABLE_RAM = (80, "Lab series doesn't have enough available RAM")
    TOO_MANY_LABS_WITHIN_THE_SPECIFIED_CLASS_ARE_CURRENTLY_ACTIVE_FOR_ANOTHER_LAB_TO_BE_LAUNCHED = (90, "Too many labs within the specified class are currently active for another lab to be launched.")
    USER_HAS_LAUNCHED_THE_MAXIMUM_NUMBER_OF_INSTANCES_OF_THIS_LAB_PROFILE = (100, "User has launched the maximum number of instances of this lab profile.")
    THIS_LAB_IS_NOT_CURRENTLY_AVAILABLE_FOR_LAUNCH_VIA_API_IT_IS_AWAITING_SECURITY_REVIEW = (110, "This lab is not currently available for launch via API. It is awaiting security review.")


