from enum import Enum


class CloudPlatformId(Enum):
    NONE = ("null", None)
    AZURE = (10, "Azure")
    AWS = (11, "AWS")


class DetailsResponseCloudPlatformIdEnum(Enum):
    AZURE = (10, "Azure")


