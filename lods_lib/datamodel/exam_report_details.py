from dataclasses import dataclass
from typing import Optional, List

from lods_lib.datamodel.exam_page import ExamPage
from lods_lib.datamodel.text_answer_response import TextAnswerResponse


@dataclass
class ExamReportDetails:
    ExamPages: Optional[List[ExamPage]] = None                     # Array containing exam pages with questions and possible answers.
    ExamAnswerResponses: Optional[List[int]] = None                 # Array containing AnswerIds the user selected from the answers in the exam pages.
    TextAnswerResponses: Optional[List[TextAnswerResponse]] = None  # Array of text based typed responses from the user.


