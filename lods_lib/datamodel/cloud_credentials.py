from dataclasses import dataclass
from typing import Optional

from dataclasses import dataclass


@dataclass
class CloudCredentials:
    PropertiesJson: Optional[str] = None  # Json serialized properties for the credentials, as defined in the cloud credential pool.
    Expires: Optional[int] = None         # When the credentials expire (in Unix epoch time).
    DisplayName: Optional[str] = None     # Friendly credential name displayed in the lab user interface.
