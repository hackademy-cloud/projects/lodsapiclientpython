from dataclasses import dataclass
from typing import Optional


@dataclass
class IpAddressInfo:
    IpAddress: Optional[str] = None            # An IP address
    MacAddress: Optional[str] = None           # The MAC address of the NIC that the IP address was assigned to.
    MachineInstanceName: Optional[str] = None  # The name of the virtual machine instance that the IP address was assigned to.
