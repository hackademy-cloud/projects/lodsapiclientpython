from dataclasses import dataclass
from typing import Optional


@dataclass
class LabInstanceSnapshot:
    Name: Optional[str] = None  # The name that the student gave to the snapshot
    Time: Optional[int] = None  # When the student created the snapshot (in Unix epoch time)
