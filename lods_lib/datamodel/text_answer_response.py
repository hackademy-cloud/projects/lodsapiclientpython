from dataclasses import dataclass
from typing import Optional


@dataclass
class TextAnswerResponse:
    Id: Optional[int] = None            # Id of the TextAnswerResponse.
    ResponseText: Optional[str] = None  # User provided response to the corrisponding question.
    IsCorrect: Optional[bool] = None    # Grading result of the user provided text response.
    AnswerId: Optional[int] = None      # Id that corrisponds to the answer object that holds the regular expression that this must match to be correct.


