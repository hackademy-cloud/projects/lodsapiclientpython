from dataclasses import dataclass
from typing import Optional


@dataclass
class Instructor:
    Id: Optional[str] = None         # The unique identifier of the instructor, as represented in your external system
    FirstName: Optional[str] = None  # The instructor's first name
    LastName: Optional[str] = None   # The instructor's last name
