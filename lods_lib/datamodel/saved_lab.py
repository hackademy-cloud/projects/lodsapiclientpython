from dataclasses import dataclass
from typing import Optional


@dataclass
class SavedLab:
    LabInstanceId: Optional[int] = None         # The unique identifier of the lab instance
    LabProfileId: Optional[int] = None          # The unique identifier of the lab profile the lab instance is based on
    LabProfileName: Optional[str] = None        # The name of the lab profile the lab instance is based on
    LabProfileNumber: Optional[str] = None      # The number/code of the lab profile the lab instance is based on
    MinutesRemaining: Optional[int] = None      # The number of minutes remaining for the student to complete the lab (when resumed)
    Saved: Optional[int] = None                 # When the lab was saved (in Unix epoch time)
    Expires: Optional[int] = None               # When the saved lab will expire (in Unix epoch time). The student must resume the lab for this date, or the lab will be deleted.
    SaveInProgress: Optional[bool] = None       # True/false indicating whether the lab is currently in the process of being saved
    IsExam: Optional[bool] = None               # Indicates whether the lab is considered a scorable exam
    SubmittedForGrading: Optional[bool] = None  # Indicates whether the lab has been submitted for grading and is in a saved state while awaiting scoring. This will only be true for exams.
