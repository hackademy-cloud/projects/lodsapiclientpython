from dataclasses import dataclass
from typing import Optional


@dataclass
class RunningLab:
    LabInstanceId: Optional[int] = None     # The unique identifier of the lab instance
    LabProfileId: Optional[int] = None       # The unique identifier of the lab profile the lab instance is based on
    LabProfileName: Optional[str] = None    # The name of the lab profile the lab instance is based on
    LabProfileNumber: Optional[str] = None  # The number/code of the lab profile the lab instance is based on
    Start: Optional[int] = None             # When the lab instance was started (in Unix epoch time)
    Expires: Optional[int] = None           # When the lab will expire (in Unix epoch time)
    Url: Optional[str] = None               # A URL where the lab can be viewed by the user
    IsExam: Optional[bool] = None           # Indicates whether the lab is considered a scorable exam
