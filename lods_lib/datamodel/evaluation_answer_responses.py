from typing import Optional

from dataclasses import dataclass


@dataclass
class EvaluationAnswerResponse:
    Number: Optional[str] = None            # The lab number.
    Name: Optional[str] = None              # The lab name.
    Tag: Optional[str] = None                   # The tag associated with the lab instance.
    Start: Optional[int] = None             # The start time of the lab instance (in Unix epoch time).
    End: Optional[int] = None               # The end time of the lab instance (in Unix epoch time).
    QuestionTypeName: Optional[str] = None  # The name of the evaluation question type.
    QuestionText: Optional[str] = None      # The text of the evaluation question.
    Label: Optional[str] = None                 # The label of the evaluation question.
    A1DText: Optional[str] = None               # The text of the 1D answer option.
    A1DWeight: Optional[int] = None             # The associated weight of the 1D answer option.
    A2DText: Optional[str] = None               # The text of the 2D answer option.
    A2DWeight: Optional[int] = None             # The associated weight of the 2D answer option.
    TextAnswer: Optional[str] = None            # The manually entered answer to the evaluation question.
