from dataclasses import dataclass
from typing import Optional, List

from lods_lib.datamodel.exam_answers import ExamAnswers


@dataclass
class ExamQuestion:
    Id: Optional[int] = None                     # Id of the ExamQuestion.
    Type: Optional[str] = None                   # The type of question. Possible values:
    Text: Optional[str] = None                   # Question text that is displayed to the user.
    SortIndex: Optional[int] = None              # Sort order for display of this question in the questions collection.
    ScoreValue: Optional[int] = None             # How many points the question is worth in the exam.
    IsRequired: Optional[bool] = None            # True if this question requires an answer.
    AnswerExplanation: Optional[str] = None      # Reason the answer is correct. This is shown to the user in the exam results.
    AnswerReferenceUrl: Optional[str] = None     # A URL reference to the reasoning for the correct answer.
    Answers: Optional[List[ExamAnswers]] = None  # Possible answers presented to the user for this question.