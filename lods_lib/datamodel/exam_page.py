from dataclasses import dataclass
from typing import Optional, List

from lods_lib.datamodel.exam_question import ExamQuestion


@dataclass
class ExamPage:
    Id: Optional[int] = None                        # Id of the ExamPage.
    Name: Optional[str] = None                      # Name of the Exam page displayed during the exam.
    SortIndex: Optional[int] = None                 # Sort order for this page in the pages collection.
    Questions: Optional[List[ExamQuestion]] = None  # Array of Exam Questions containing data for all questions in this page.


