from dataclasses import dataclass
from typing import Optional


@dataclass
class ReplacementToken:
    Token: Optional[str] = None        # The token name
    Replacement: Optional[str] = None  # The replacement value for the token
