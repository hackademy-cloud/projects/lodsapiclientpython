from dataclasses import dataclass
from typing import Optional


@dataclass
class ExamAnswers:
    Id: Optional[int] = None          # Id of the ExamAnswer
    Text: Optional[str] = None        # Answer text displayed to the user.
    IsCorrect: Optional[bool] = None  # Is true if this is the correct answer or one of the correct answers.
    SortIndex: Optional[int] = None   # Sort order for display of this answer in the answer collection
