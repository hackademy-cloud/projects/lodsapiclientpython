from dataclasses import dataclass
from typing import Optional

from dataclasses import dataclass


@dataclass
class LabSeries:
    Id: Optional[int] = None               # The unique identifier of the lab series
    Name: Optional[str] = None             # The name of the lab series
    Description: Optional[str] = None      # A brief description of the lab series
    NumTrainingDays: Optional[int] = None  # The number of training days expected to complete the series