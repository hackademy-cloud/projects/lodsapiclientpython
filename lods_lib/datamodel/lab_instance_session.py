from typing import Optional

from dataclasses import dataclass


@dataclass
class LabInstanceSession:
    Start: Optional[int] = None  # When the session started (in Unix epoch time)
    End: Optional[int] = None        # When the session ended (in Unix epoch time)
