from dataclasses import dataclass
from typing import Optional


@dataclass
class LabInstance:
    Id: Optional[int] = None                # The unique identifier of the lab instance
    UserId: Optional[str] = None            # The ID of the user the lab instance belongs to
    UserFirstName: Optional[str] = None     # The first name of the user the lab instance belongs to
    UserLastName: Optional[str] = None      # The last name of the user the lab instance belongs to
    LabProfileId: Optional[int] = None      # The unique identifier of the lab profile the lab instance is based on
    LabProfileNumber: Optional[str] = None  # The number/code of the lab profile the lab instance is based on
    LabProfileName: Optional[str] = None    # The name of the lab profile the lab instance is based on
    Start: Optional[int] = None             # When the lab instance was started (in Unix epoch time)
    Expires: Optional[int] = None           # When the lab will expire (in Unix epoch time)
    IsExam: Optional[bool] = None           # Indicates whether the lab is considered a scorable exam
