from dataclasses import dataclass
from typing import Optional

from dataclasses import dataclass


@dataclass
class DeliveryRegion:
    Id: Optional[int] = None                            # The unique identifier of the delivery region
    Name: Optional[str] = None                          # The name of the delivery region
    Description: Optional[str] = None  # A brief description of the delivery region
