from lods_lib.client import LodsApiClient
from lods_lib.api_methods.launch import LaunchParameters, LaunchResponse
from lods_lib.enums.result_and_status import LaunchResponseResult
from lods_lib.errors.lods_errors import LodsResponseError


def example_launch(client: LodsApiClient):
    params = LaunchParameters(
        labId=12001,
        userId="sample.student@nowhere.net",
        # you can use any value that will uniquely identify the user in your system. here, we use an email address.
        firstName="Sample",
        lastName="Student",
        email="sample.student@nowhere.net")
    try:
        launch_response: LaunchResponse = client.launch(params)
    except LodsResponseError as err:
        print("An error occurred:")
        print(err)
        print("Exiting Example")
        return
    print(launch_response)
    if launch_response.Result == LaunchResponseResult.SUCCESS.value[0]:
        url = launch_response.Url  # this is the URL to send the user to
    else:
        error = launch_response.Error  # this will explain what went wrong


def example(api_key: str, real_api: bool=True):
    if real_api:
        # running lods_fake_api on localhost:5001
        api_url = "http://localhost:5001/api/{method}"
        client = LodsApiClient(api_key, api_url)
    else:
        client = LodsApiClient(api_key)
    example_launch(client)


if __name__ == '__main__':
    api_key = "YourKeyGoesHere"
    example(api_key, False)