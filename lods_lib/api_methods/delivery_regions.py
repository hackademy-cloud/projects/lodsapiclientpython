from dataclasses import dataclass
from typing import Optional, List

from lods_lib.datamodel.delivery_region import DeliveryRegion


@dataclass
class DeliveryRegionsResponse:
    DeliveryRegions: Optional[List[DeliveryRegion]] = None  # See the DeliveryRegion Type below
    Error: Optional[str] = None
    Status: Optional[int] = None
