from dataclasses import dataclass
from typing import Optional


@dataclass
class DeleteClassParameters:
    id: str  # The unique identifier of the class, as represented in your organization


@dataclass
class DeleteClassResponse:
    Success: Optional[bool] = None
    Error: Optional[str] = None
    Status: Optional[int] = None
