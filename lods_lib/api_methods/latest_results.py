from dataclasses import dataclass
from typing import Optional, List

from lods_lib.datamodel.lab_result import LabResult


@dataclass
class LatestResultsParameters:
    minutes: int  # Lab instances that have changed state within this many minutes of the current time will be included. This value cannot exceed 10,080 (7 days).|


@dataclass
class LatestResultsResponse:
    Results: Optional[List[LabResult]] = None  # See LabResult type below
    Error: Optional[str] = None
    Status: Optional[int] = None
