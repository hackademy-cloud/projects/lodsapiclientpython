from dataclasses import dataclass
from typing import Optional, List

from lods_lib.datamodel.running_lab import RunningLab
from lods_lib.datamodel.saved_lab import SavedLab


@dataclass
class UserRunningAndSavedLabsParameters:
    userId: str  # The unique identifier used to identify the user in within your organization


@dataclass
class UserRunningAndSavedLabsResponse:
    RunningLabs: Optional[List[RunningLab]] = None  # See the RunningLab Type below
    SavedLabs: Optional[List[SavedLab]] = None      # See the SavedLab Type below
    Error: Optional[str] = None
    Status: Optional[int] = None
