from dataclasses import dataclass
from typing import Optional, List


@dataclass
class UpdateClassParameters:
    id: str                                      # The unique identifier of the class, as represented in your organization
    name: str                                    # The name of the class
    start: int                                   # When the class starts (in Unix epoch time)
    end: int                                     # When the class ends (in Unix epoch time)
    expires: int                                 # When labs can no longer be associated with the class (in Unix epoch time)
    instructorFirstName: Optional[str] = None    # The first name of the class instructor
    instructorLastName: Optional[str] = None     # The last name of the class instructor
    maxActiveLabInstances: Optional[int] = None  # The maximum number of active lab instances than can exist concurrently within this class context. This is optional in most situations. However, it is required for classes that host shared environments. If a shared lab is launched against a class that doesn't have this value set, the launch will fail.
    availableLabs: Optional[List[int]] = None    # An optional array of lab profile IDs. When provided, Lab on Demand will know that these labs are available within the class. You do not need to set this value in order to launch labs against the class. This is useful when using Lab on Demand to display a class attendance page, or when consuming shared class environments and you want the shared environment launch link to appear on the class monitor page.


@dataclass
class UpdateClassResponse:
    Success: Optional[bool] = None
    Error: Optional[str] = None
    Status: Optional[int] = None
