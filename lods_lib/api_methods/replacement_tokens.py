from dataclasses import dataclass
from typing import Optional, List

from lods_lib.datamodel.replacement_token import ReplacementToken


@dataclass
class ReplacementTokensParameters:
    Id: int  # The ID of the lab instance


@dataclass
class ReplacementTokensResponse:
    ReplacementTokens: Optional[List[ReplacementToken]] = None  # See the ReplacementToken Type below
    Error: Optional[str] = None
    Status: Optional[int] = None
