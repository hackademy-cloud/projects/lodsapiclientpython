from dataclasses import dataclass
from typing import Optional

"""
## Package File Format
- Standard ZIP archive
- **Instructions.md** file in the archive root
- You can include other content files referenced within your instructions, like images and videos.

Content files are referenced in your instructions markdown using relative paths. You can use subfolders if you wish. For instance, if your package contains the following files...

```nocopy-nocolor
image1.jpg
image2.jpg
screenshots/screenshot1.jpg
screenshots/screenshot2.jpg
documents/document1.pdf
documents/document2.pdf
```

You could reference these content files in your instructions like this...

```nocopy-nocolor
![](image1.jpg)
![](image2.jpg)
![](screenshots/screenshot1.jpg)
![](screenshots/screenshot2.jpg)
[Download document 1](documents/document1.pdf)
[Download document 2](documents/document2.pdf)
```
"""


@dataclass
class UpdateLabInstructionsPackageParameters:
    id: int  # The ID of the lab profile to update.


@dataclass
class UpdateLabInstructionsPackageResponse:
    Result: Optional[int] = None
    Error: Optional[str] = None  # In the event of an error, this will contain a detailed error message.
    Status: Optional[int] = None
