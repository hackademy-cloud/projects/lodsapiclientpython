from enum import Enum
from dataclasses import dataclass
from typing import Optional


@dataclass
class CloseUserAccountParameters:
    userId: str  # The ID of the user to retrieve. This is usually a unique identifier used by the calling system.


@dataclass
class CloseUserAccountResponse:
    Status: Optional[int] = None        # Indicates the status of the API request
    Error: Optional[str] = None         # Error details. This will only have a value if an error was encountered. The status property will also be set to Error (0).
    RowsAffected: Optional[int] = None  # The number of database rows affected while removing all user data for the account.
