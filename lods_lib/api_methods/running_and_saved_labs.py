from dataclasses import dataclass
from typing import Optional, List

from lods_lib.datamodel.lab_instance import LabInstance


@dataclass
class RunningAndSavedLabsResponse:
    RunningLabs: Optional[List[LabInstance]] = None  # See the LabInstance Type below
    SavedLabs: Optional[List[LabInstance]] = None    # See the LabInstance Type below
    Error: Optional[str] = None
    Status: Optional[int] = None
