from dataclasses import dataclass
from typing import Optional, List

from lods_lib.datamodel.evaluation_answer_responses import EvaluationAnswerResponse


@dataclass
class GetEvaluationResponsesForEventParameters:
    id: int  # The unique identifier of the event.


@dataclass
class GetEvaluationResponsesForEventResponse:
    Responses: Optional[List[EvaluationAnswerResponse]] = None  # See the EvaluationAnswerResponse Type below
    Error: Optional[str] = None
    Status: Optional[int] = None
