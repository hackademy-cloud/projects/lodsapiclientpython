from dataclasses import dataclass
from typing import Optional, List

from lods_lib.datamodel.evaluation_answer_responses import EvaluationAnswerResponse


@dataclass
class GetEvaluationResponsesParameters:
    id: int                            # The unique identifier of the evaluation.
    takenAfter: Optional[int] = None   # The date/time (in Unix epoch time) that the evaluation response is after.
    takenBefore: Optional[int] = None  # The date/time (in Unix epoch time) that the evaluation response is after.
    classId: Optional[int] = None      # The unique identifier of the class the evaluation was taken in.
    eventId: Optional[int] = None      # The unique identifier of the event the evaluation was taken in.


@dataclass
class GetEvaluationResponsesResponse:
    Responses: Optional[List[EvaluationAnswerResponse]] = None  # See the EvaluationAnswerResponse Type below
    Error: Optional[str] = None
    Status: Optional[int] = None
