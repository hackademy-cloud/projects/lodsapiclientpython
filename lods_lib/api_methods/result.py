from enum import Enum
from lods_lib.datamodel.exam_report_details import ExamReportDetails
from lods_lib.datamodel.lab_result import LabResult
from dataclasses import dataclass
from typing import Optional, List


@dataclass
class ResultParameters:
    labInstanceId: int  # The ID of the lab instance


@dataclass
class ResultResponse:
    """Is equal to LabResult but with more keys."""
    LabProfileId: Optional[int] = None         # The unique identifier of the lab profile the lab is based on
    Start: Optional[int] = None                # When the lab instance was started (in Unix epoch time)
    End: Optional[int] = None                      # When the lab instance was started (in Unix epoch time)
    UserId: Optional[str] = None                   # The user ID as represented in your organization
    ClassId: Optional[str] = None                  # The class ID as represented in your organization
    CompletionStatus: Optional[int] = None
    TotalRunTimeSeconds: Optional[int] = None  # The total number of seconds the lab was running, whether or not the student was present.
    TaskCompletePercent: Optional[int] = None  # If the lab has integrated tasks, the percentage of tasks that the user has completed.
    IsExam: Optional[bool] = None              # Indicates whether the lab is scored as an exam
    ExamPassed: Optional[bool] = None              # Indicates whether the user passed the exam. Will only be set if the lab is an exam (IsExam = true) and the exam has been scored.
    ExamScore: Optional[int] = None                # Indicates the exam score. Will only be set if the lab is an exam (IsExam = true) and the exam has been scored.
    ExamMaxPossibleScore: Optional[int] = None     # Indicates the exam maximum possible score. Will only be set if the lab is an exam (IsExam = true) and the exam has been scored.
    ExamPassingScore: Optional[int] = None         # Indicates the minimum score required to pass the exam. Will only be set if the lab is an exam (IsExam = true) and the exam has been scored.
    IpAddress: Optional[str] = None                # The user's IP address. This is only included if the IP address was provided when the lab was launched.
    Country: Optional[str] = None                  # The user's country as determined by IP address geolocation. This is only included if the IP address was provided when the lab was launched.
    Region: Optional[str] = None                   # The user's state/region as determined by IP address geolocation. This is only included if the IP address was provided when the lab was launched.
    City: Optional[str] = None                     # The user's city as determined by IP address geolocation. This is only included if the IP address was provided when the lab was launched.
    Latitude: Optional[float] = None               # The user's latitude as determined by IP address geolocation. This is only included if the IP address was provided when the lab was launched.
    Longitude: Optional[float] = None              # The user's longitude as determined by IP address geolocation. This is only included if the IP address was provided when the lab was launched.
    LabHostId: Optional[int] = None                # The ID of the lab host server that the lab instance is housed on.
    DatacenterId: Optional[int] = None             # The ID of the datacetner that the lab instance is located in.
    DeliveryRegionId: Optional[int] = None         # The ID of the delivery region that the lab instance is located in.
    ExamDetails: Optional[List[ExamReportDetails]] = None  # An array of exam details containing exam questions, answers, and score results
    Error: Optional[str] = None
    Status: Optional[int] = None
