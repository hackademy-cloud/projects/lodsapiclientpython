from lods_lib.datamodel.instructor import Instructor as InstructorAlias
from dataclasses import dataclass
from typing import Optional


@dataclass
class ClassParameters:
    id: str  # The unique identifier of the class, as represented in your organization


@dataclass
class ClassResponse:
    Id: Optional[str] = None                 # The unique identifier of the class, as represented in your organization
    Name: Optional[str] = None               # The name of the class
    Start: Optional[int] = None              # When the class starts (in Unix epoch time)
    End: Optional[int] = None                # When the class ends (in Unix epoch time)
    Expires: Optional[int] = None            # When labs can no longer be associated with the class (in Unix epoch time)
    Instructor: Optional[InstructorAlias] = None  # The class instructor. See the Instructor Type below.
    Url: Optional[str] = None                # A URL where the class can be viewed
    Error: Optional[str] = None
    Status: Optional[int] = None
