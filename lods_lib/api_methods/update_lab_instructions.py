from dataclasses import dataclass
from typing import Optional


@dataclass
class UpdateLabInstructionsParameters:
    id: int            # The ID of the lab profile to update.
    instructions: str  # The IDL-MD instructions. This should included in the body of the post.


@dataclass
class UpdateLabInstructionsResponse:
    Result: Optional[int] = None
    Error: Optional[str] = None  # In the event of an error, this will contain a detailed error message.
    Status: Optional[int] = None
