from dataclasses import dataclass
from typing import Optional, List

from lods_lib.datamodel.lab_series import LabSeries as LabSeriesAlias
from lods_lib.datamodel.delivery_region import DeliveryRegion
from lods_lib.datamodel.lab_profile import LabProfile


@dataclass
class CatalogParameters:
    includeAll: Optional[int] = None  # This parameter can usually be ignored. When not included (or passed as any value except 1), labs that are not currently available for launch will not be included in catalog results.If you want to include all lab profiles, regardless of whether they are enabled or are developmentally complete, you can pass includeAll=1.


@dataclass
class CatalogResponse:
    LabSeries: Optional[List[LabSeriesAlias]] = None             # See the LabSeries Type below
    LabProfiles: Optional[List[LabProfile]] = None          # See the LabProfile Type below
    DeliveryRegions: Optional[List[DeliveryRegion]] = None  # See the DeliveryRegion Type below
    Error: Optional[str] = None
    Status: Optional[int] = None
