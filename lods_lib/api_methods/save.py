from dataclasses import dataclass
from typing import Optional


@dataclass
class SaveParameters:
    labInstanceId: int  # The ID of the lab instance to save


@dataclass
class SaveResponse:
    Result: Optional[int] = None
    Expires: Optional[int] = None  # When the saved lab will expire (in Unix epoch time)
    Error: Optional[str] = None    # In the event of an error, this will contain a detailed error message.
    Status: Optional[int] = None
