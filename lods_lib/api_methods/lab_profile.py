from dataclasses import dataclass
from typing import Optional, List


@dataclass
class LabProfileParameters:
    Id: int  # The unique identifier of the lab profile.


@dataclass
class LabProfileResponse:
    Id: Optional[int] = None                        # The unique identifier of the lab profile
    Name: Optional[str] = None                      # The name of the lab profile
    Number: Optional[str] = None                        # The lab number (usually to identify a lab within a series, e.g. Module 1, Module 2, etc.)
    PlatformId: Optional[int] = None                # The virtualization platform the lab is run on.
    CloudPlatformId: Optional[int] = None           # The cloud platform the lab is run on. Not optional, but can be null.
    SeriesId: Optional[int] = None                      # The unique identifier of the series the lab profile belongs to
    Enabled: Optional[bool] = None                  # Whether the lab is currently enabled for launch.
    ReasonDisabled: Optional[str] = None                # The reason the lab is disabled. Only supplied when the lab is not enabled.
    DevelopmentStatusId: Optional[int] = None       # Indicates the development status of the lab. In general, a lab not marked as Complete should not be launched (though it can be).
    Description: Optional[str] = None                   # A brief description of the lab profile
    Objective: Optional[str] = None                     # Text describing the objective of the lab
    Scenario: Optional[str] = None                      # Text describing the scenario of the lab
    ExpectedDurationMinutes: Optional[int] = None   # The expected number of minutes a user will take to complete the lab
    DurationMinutes: Optional[int] = None           # The maximum number of minutes a lab instance is allowed to run before it expires
    RAM: Optional[int] = None                       # The amount of RAM in MB used by the lab
    HasIntegratedContent: Optional[bool] = None     # Indicates whether the lab has integrated digital lab (IDL) content
    ContentVersion: Optional[int] = None            # Indicates the content version (only applicable if HasIntegratedContent = true)
    IsExam: Optional[bool] = None                   # Indicates whether the lab is scored as an exam
    PremiumPrice: Optional[float] = None            # The consumption cost of the lab when premium experience features are included.
    BasicPrice: Optional[float] = None              # The consumption cost of the lab when only basic experience features are included.
    Tags: Optional[List[str]] = None                # A list of tags associated with the lab profile.
    SharedClassEnvironmentRoleId: Optional[int] = None  # Indicates the role the lab plays in a shared environment
    Error: Optional[str] = None
    Status: Optional[int] = None
