from dataclasses import dataclass
from typing import Optional


@dataclass
class ScoreActivitiesParameters:
    Id: Optional[int] = None  # The ID of the lab instance


@dataclass
class ScoreActivitiesResponse:
    Success: Optional[bool] = None
    Error: Optional[str] = None
    Status: Optional[int] = None
