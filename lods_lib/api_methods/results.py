from dataclasses import dataclass
from typing import Optional, List

from lods_lib.datamodel.lab_result import LabResult


@dataclass
class ResultsParameters:
    start: int  # The start of the timeframe to check against (in Unix epoch time)
    end: int    # The end of the timeframe to check against (in Unix epoch time)


@dataclass
class ResultsResponse:
    Results: Optional[List[LabResult]] = None  # See LabResult type below
    Error: Optional[str] = None
    Status: Optional[int] = None
