from dataclasses import dataclass
from typing import Optional


@dataclass
class ResumeParameters:
    labInstanceId: int  # The ID of the lab instance to resume


@dataclass
class ResumeResponse:
    Result: Optional[int] = None
    Url: Optional[str] = None      # A URL where the lab can be viewed by the user
    Expires: Optional[int] = None  # When the saved lab will expire (in Unix epoch time)
    Error: Optional[str] = None    # In the event of an error, this will contain a detailed error message.
    Status: Optional[int] = None
