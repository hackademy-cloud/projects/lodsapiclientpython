from dataclasses import dataclass
from typing import Optional


@dataclass
class StatsResponse:
    NumActive: Optional[int] = None  # The number of currently active labs (starting, running, tearing down, saving, resuming, etc)
    NumSaved: Optional[int] = None   # The number of currently saved labs
    Error: Optional[str] = None
    Status: Optional[int] = None
