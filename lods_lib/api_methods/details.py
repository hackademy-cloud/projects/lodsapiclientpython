from enum import Enum
from dataclasses import dataclass
from typing import Optional, List

from lods_lib.datamodel.ip_address_info import IpAddressInfo
from lods_lib.datamodel.lab_instance_session import LabInstanceSession
from lods_lib.datamodel.lab_instance_snapshot import LabInstanceSnapshot
from lods_lib.datamodel.cloud_credentials import CloudCredentials as CloudCredentialsAlias


@dataclass
class DetailsParameters:
    labInstanceId: int  # The ID of the lab instance


@dataclass
class DetailsResponse:
    Id: Optional[int] = None                   # The ID of the lab instance
    LabProfileId: Optional[int] = None         # The ID of the lab profile
    LabProfileName: Optional[str] = None       # The name of the lab profile
    SeriesId: Optional[int] = None                 # The ID of the lab series.
    SeriesName: Optional[str] = None               # The name of the lab series.
    UserId: Optional[str] = None               # The ID you use to identify the user in your external system.
    UserFirstName: Optional[str] = None        # The user's first name.
    UserLastName: Optional[str] = None         # The user's last name.
    ClassId: Optional[str] = None                  # The ID you use to identify the associated class in your external system.
    ClassName: Optional[str] = None                # The name of the class the lab instance is associated with.
    Start: Optional[int] = None                # When the lab was started (in Unix epoch time).
    Expires: Optional[int] = None              # When the lab expires (in Unix epoch time).
    End: Optional[int] = None                      # When the lab ended (in Unix epoch time).
    LastActivity: Optional[int] = None             # When student activity was last detected (in Unix epoch time).
    LastSave: Optional[int] = None                 # When the lab was last saved (in Unix epoch time).
    SaveExpires: Optional[int] = None              # If the lab instance is saved, when the saved data will expire and be deleted (in Unix epoch time).
    State: Optional[str] = None                # The state of the lab instance. Possible values:
    CompletionStatus: Optional[str] = None     # The student's completion status. Possible values:
    PoolMemberName: Optional[str] = None           # If the lab contains a virtual machine pool, the name of the pool member that was used.
    Platform: Optional[str] = None             # The platform the lab has hosted on. Possible values:
    CloudPlatformId: Optional[int] = None
    LabHostId: Optional[int] = None            # The ID of the lab host machine.
    LabHostName: Optional[str] = None          # The name of the lab host machine.
    DatacenterId: Optional[int] = None         # The ID of the datacenter where the lab is located.
    DatacenterName: Optional[str] = None       # The name of the datacenter where the lab is located.
    DeliveryRegionId: Optional[int] = None     # The ID of the delivery region where the lab is located.
    DeliveryRegionName: Optional[str] = None   # The name of the delivery region where the lab is located.
    RemoteController: Optional[str] = None     # The name of the remote controller used by the user.
    BrowserUserAgent: Optional[str] = None         # The browser user agent used by the user.
    LastLatency: Optional[int] = None              # The last known latency value as measured between the client and the lab's datacenter.
    LastSaveTriggerType: Optional[str] = None      # If the lab is currently saved, what triggered the save operation. Possible values:
    TotalRunTime: Optional[int] = None         # The total number of seconds the lab was running, whether or not the student was present.
    TimeInSession: Optional[int] = None        # The total number of seconds the user spent in the lab.
    TimeRemaining: Optional[int] = None        # The total number of seconds remaining before the lab expires.
    InstructorName: Optional[str] = None           # The name of the instructor for the associated class.
    StartupDuration: Optional[int] = None          # The number of seconds it took the lab to start.
    HasContent: Optional[bool] = None          # Indicates whethere the lab has content, or simply houses virtual machines.
    IsExam: Optional[bool] = None              # Indicates whether the lab is scored as an exam
    ExamPassed: Optional[bool] = None              # Indicates whether the user passed the exam. Will only be set if the lab is an exam (IsExam = true) and the exam has been scored.
    ExamScore: Optional[int] = None                # Indicates the exam score. Will only be set if the lab is an exam (IsExam = true) and the exam has been scored.
    ExamMaxPossibleScore: Optional[int] = None     # Indicates the exam maximum possible score. Will only be set if the lab is an exam (IsExam = true) and the exam has been scored.
    ExamPassingScore: Optional[int] = None         # Indicates the minimum score required to pass the exam. Will only be set if the lab is an exam (IsExam = true) and the exam has been scored.
    ExamScoredById: Optional[int] = None           # The ID of the user that manually scored the exam. Will only be set if the lab is an exam (IsExam = true) and the exam has been scored manually. Automatically scored exams will not include a value for this property.
    ExamScoredByName: Optional[str] = None         # The name of the user that manually scored the exam. Will only be set if the lab is an exam (IsExam = true) and the exam has been scored manually. Automatically scored exams will not include a value for this property.
    ExamScoredTime: Optional[int] = None           # When the exam was scored (in Unix epoch time). Will only be set if the lab is an exam (IsExam = true) and the exam has been scored.
    Task: Optional[str] = None                     # If the lab has content (HasContent=true), indicates the name of the task the student is working on.
    Exercise: Optional[str] = None                 # If the lab has content (HasContent=true), indicates the name of the exercise the student is working on.
    NumTasks: Optional[int] = None             # If the lab has content (HasContent=true), indicates the total number of tasks in the lab.
    NumCompletedTasks: Optional[int] = None    # If the lab has content (HasContent=true), indicates the number of tasks the student has completed.
    TaskCompletePercent: Optional[int] = None  # If the lab has content (HasContent=true), indicates the percentage of tasks that the student has completed.
    MonitorUrl: Optional[str] = None               # If the lab is currently running, a URL at which the lab can be monitored in real time.
    Errors: Optional[List[str]] = None         # An array of all errors associated with the lab instance.
    IpAddress: Optional[str] = None                # The user's IP address. This is only included if the IP address was provided when the lab was launched.
    Country: Optional[str] = None                  # The user's country as determined by IP address geolocation. This is only included if the IP address was provided when the lab was launched.
    Region: Optional[str] = None                   # The user's state/region as determined by IP address geolocation. This is only included if the IP address was provided when the lab was launched.
    City: Optional[str] = None                     # The user's city as determined by IP address geolocation. This is only included if the IP address was provided when the lab was launched.
    Latitude: Optional[float] = None               # The user's latitude as determined by IP address geolocation. This is only included if the IP address was provided when the lab was launched.
    Longitude: Optional[float] = None              # The user's longitude as determined by IP address geolocation. This is only included if the IP address was provided when the lab was launched.
    Snapshots: Optional[List[LabInstanceSnapshot]] = None      # An array of snapshots created by the user within the lab. See the LabInstanceSnapshot Type below.
    Sessions: Optional[List[LabInstanceSession]] = None        # An array of session times the student spent in the lab. See the LabInstanceSnapshot Type below.
    PublicIpAddresses: Optional[List[IpAddressInfo]] = None    # An array of public IP address information objects. See the IpAddressInfo Type below.
    CloudCredentials: Optional[List[CloudCredentialsAlias]] = None  # An array of credentials assigned to the lab instance. See the CloudCredentials Type below.
    Error: Optional[str] = None
    Status: Optional[int] = None
