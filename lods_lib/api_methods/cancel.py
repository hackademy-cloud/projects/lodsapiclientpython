from dataclasses import dataclass
from typing import Optional


@dataclass
class CancelParameters:
    labInstanceId: int  # The ID of the lab instance to cancel


@dataclass
class CancelResponse:
    Result: Optional[int] = None
    Error: Optional[str] = None  # In the event of an error, this will contain a detailed error message.
    Status: Optional[int] = None
