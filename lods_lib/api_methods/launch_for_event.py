from dataclasses import dataclass
from typing import Optional


@dataclass
class LaunchForEventParameters:
    labId: int                          # The ID of the lab profile
    eventId: int                        # The ID of the event the lab is part of.
    userId: str                         # The ID you use to identify the user in your external system.
    firstName: str                      # The user’s first name
    lastName: str                       # The user’s last name
    email: str                          # The user’s email address
    classId: Optional[str] = None              # An optional parameter used to associate the lab with a class (see GetOrCreateClass). This is the unique identifier of the class as it is represented in your organization.
    canBeMarkedComplete: Optional[int] = None  # An optional parameter used to specify if the lab can be marked as complete by the student. 1 = true, 2 = false. If not specified, defaults to 1 (true).
    tag: Optional[str] = None                  # An optional parameter that can be used for tagging the lab instance with your own custom data.
    ipAddress: Optional[str] = None            # When specified, Lab on Demand will attempt to launch the lab in the closest available delivery region. You should provide the IP address of the user that is taking the lab, not the IP address of your system.
    regionId: Optional[int] = None             # When specified, Lab on Demand will attempt to launch the lab in the specified delivery region. Delivery regions can be found using the [DeliveryRegions](lod-api-delivery-regions.md) command or [Catalog](lod-api-catalog.md) command.


@dataclass
class LaunchForEventResponse:
    Result: Optional[int] = None
    Url: Optional[str] = None            # A URL where the lab can be viewed by the user
    LabInstanceId: Optional[int] = None  # The Id assigned to the new lab instance
    Expires: Optional[int] = None        # When the lab will expire (in Unix epoch time)
    Error: Optional[str] = None          # In the event of an error, this will contain a detailed error message.
    Status: Optional[int] = None
