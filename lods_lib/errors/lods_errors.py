from requests import Response


class LodsResponseError(Exception):
    def __init__(self, Error: str, Status: int):
        self.Error = Error
        self.Status = Status
        super().__init__(self.Error)


class LodsResponseException(Exception):
    def __init__(self, response: Response, exception1: Exception, exception2: Exception):
        self.response = response
        self.exception1 = exception1
        self.exception2 = exception2