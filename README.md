# Learn on Demand Systems API Client

This is a Client for the [Learn on Demand Systems (Lods) API](https://github.com/LearnOnDemandSystems/docs/blob/master/lod/lod-api/lod-api-main.md).

## Version
This client works against the api version documented on this commit:
https://github.com/LearnOnDemandSystems/docs/commit/d46771478ace4a400153e9efbabbc81b42542671 (Jul 16, 2020)

## API Key
The client requires an API key, which must be obtained from
Learn on Demand Systems.

## API Client

This client will allow you to use the API using python code.

### Example:

```python
from lods_lib.client import LodsApiClient
from lods_lib.api_methods.launch import LaunchParameters, LaunchResponse
from lods_lib.enums.result_and_status import LaunchResponseResult

api_url = "https://labondemand.com/api/v3/{method}"
api_key = "YourKeyGoesHere"

client = LodsApiClient(api_key, api_url)

params = LaunchParameters(
    labId=12001,
    userId="sample.student@nowhere.net",  # you can use any value that will uniquely identify the user in your system. here, we use an email address.
    firstName="Sample",
    lastName="Student",
    email="sample.student@nowhere.net")

launch_response: LaunchResponse = client.launch(params)
print(launch_response)

if launch_response.Result == LaunchResponseResult.SUCCESS.value[0]:
    url = launch_response.Url  # this is the URL to send the user to
else:
    error = launch_response.Error  # this will explain what went wrong
```
